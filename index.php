<?php
require  __DIR__ .'/vendor/autoload.php';
use LogFile\LogReader;
use LogFile\LogProcessor;
use LogFile\ErrorLogger;
use LogFile\OutputterJSON;


$argv = $_SERVER['argv'];
if(count($argv) != 2){
    die("Expecting filename to be passed as commandline parameter, e.g. ".basename(__FILE__)." file.log\n");
}
$file = $argv[1];

if (!is_file($file)) {
    die("Please enter valid log file.\n");
}

$errorFile = $file.'.err';
$outputFile = pathinfo($argv[1])['filename'].'.json';

$logReader = new LogReader($file);
$errorLogger = new ErrorLogger($errorFile);
$outputter = new OutputterJSON($outputFile);

$lp = new LogProcessor($logReader, $errorLogger, $outputter);
$lp->process();

