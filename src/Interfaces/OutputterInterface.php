<?php
namespace LogFile\Interfaces;

interface OutputterInterface
{
    public function generate(array $output);
}
