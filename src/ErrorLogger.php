<?php

namespace LogFile;

class ErrorLogger
{
    private $errorFile;

    public function __construct(string $errorFile)
    {
        $this->errorFile = $errorFile;
        if (file_exists($this->errorFile)) {
            unlink($this->errorFile);
        }
    }

    public function log(string $error)
    {
        file_put_contents($this->errorFile, $error, FILE_APPEND);
    }
}
