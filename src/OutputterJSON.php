<?php

namespace LogFile;

use LogFile\Interfaces\OutputterInterface;

class OutputterJSON implements OutputterInterface
{
    private $outputFile;

    public function __construct(string $outputFile)
    {
        $this->outputFile = $outputFile;
    }

    public function generate(array $data)
    {
        file_put_contents($this->outputFile, json_encode($data), FILE_APPEND);
    }
}
