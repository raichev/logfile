<?php

namespace LogFile;

use DateTime;
use Exception;

class LogProcessor
{
    const STATUS_FIELD = 7;
    const TIME_FIELD = 1;
    const DATE_FIELD = 0;
    const URL_FIELD = 10;
    const MAX_LINE_FIELDS = 18;
    const BYTE_FIELD = 8;
    const MISS_STRING_POSITION = 1;
    const MISS_STRING_POSITION_IN_TCP_EXPIRED_MISS = 2;
    const STATUS_TYPE = 0;
    const STATUS_NUMBER = 1;

    private $logReader;
    private $errorLogger;
    private $outputter;

    public function __construct(LogReader $logReader, ErrorLogger $errorLogger, OutputterJSON $outputter)
    {
        $this->logReader = $logReader;
        $this->errorLogger = $errorLogger;
        $this->outputter = $outputter;
    }

    public function process()
    {
        $fieldsArray = [];
        $rowNumber = 0;
        foreach ($this->logReader->readFile() as $row) {
            $rowNumber++;
            if ($rowNumber!= 1) {
                $fieldsArray = $this->checkRow($row, $fieldsArray, $rowNumber);
            }
        }

        $this->outputter->generate($fieldsArray);
    }

    private function checkRow($row, $fieldsArray, $rowNumber)
    {
        $row = str_getcsv($row, ' ');
        try {
            return $this->createArrayBasedOnRowFields($row, $fieldsArray, $rowNumber);
        } catch (Exception $e) {
            $this->errorLogger->log($e->getMessage());
            return $fieldsArray;
        }
    }

    private function createArrayBasedOnRowFields(array $line, array $fieldsArray, string $rowNumber)
    {
        if (count($line) === self::MAX_LINE_FIELDS) {
            $host = $this->getHost($line, $rowNumber);
            $time = $this->getTime($line, $rowNumber);
            $httpStatus = $this->getHttpStatus($line, $rowNumber);
            $cacheStatus = $this->getHttpCacheStatus($line);
            $date = $this->getDate($line, $rowNumber);
            $pointer = &$fieldsArray[$host][$date][$time][$httpStatus][$cacheStatus];
            $pointer['requests'] = isset($pointer['requests']) ? $pointer['requests'] + 1 : 1;
            $pointer['bytes'] =
                isset($pointer['bytes'])
                    ? $pointer['bytes'] + (int)$line[self::BYTE_FIELD] : (int)$line[self::BYTE_FIELD];
        } else {
            throw new Exception('Expecting 18 fields but received '.count($line).' on line '.$rowNumber.PHP_EOL);
        }

        return $fieldsArray;
    }

    private function getDate(array &$line, string $rowNumber)
    {
        $date = $line[self::DATE_FIELD];
        if (!DateTime::createFromFormat('Y-m-d', $date)) {
            throw new Exception('Invalid date on line ' . $rowNumber.PHP_EOL);
        } else {
            return $date;
        }
    }

    private function getTime(array &$line, string $rowNumber)
    {
        $time = $line[self::TIME_FIELD];
        if (!DateTime::createFromFormat('h:i:s', $time)) {
            throw new Exception('Invalid time on line ' . $rowNumber.PHP_EOL);
        } else {
            return substr($time, 0, 4) . '0';
        }
    }

    private function getHost(array &$line, string $rowNumber)
    {
        $parseUrl = parse_url($line[self::URL_FIELD]);
        if (!isset($parseUrl['host'])) {
            throw new Exception('Invalid url on line '. $rowNumber.PHP_EOL);
        } else {
            return $parseUrl['host'];
        }
    }

    private function getHttpStatus(array &$line, string $rowNumber)
    {
        $httpStatus = $line[self::STATUS_FIELD];
        $splitStatus = explode('/', $httpStatus);
        if (strpos($line[self::STATUS_FIELD], 'ERROR') !== false) {
            throw new Exception('Invalid cache status in '.$httpStatus.' on line '. $rowNumber.PHP_EOL);
        } else {
            return $splitStatus[self::STATUS_NUMBER];
        }
    }

    private function getHttpCacheStatus(array &$line)
    {
        $splitStatus = explode('/', $line[self::STATUS_FIELD]);
        $explodedType = explode('_', $splitStatus[self::STATUS_TYPE]);
        $type =
            isset($explodedType[self::MISS_STRING_POSITION_IN_TCP_EXPIRED_MISS])
                    ? strtolower($explodedType[self::MISS_STRING_POSITION_IN_TCP_EXPIRED_MISS])
                    : strtolower($explodedType[self::MISS_STRING_POSITION]);

        return $type;
    }
}
