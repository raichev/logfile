<?php

namespace LogFile;

class LogReader
{
    private $fileName;
    private $file;

    public function __construct(string $fileName)
    {
        $this->fileName = $fileName;

        $this->file = fopen($this->fileName, 'r');
    }

    public function __destruct()
    {
        fclose($this->file);
    }

    private function getRow()
    {
        return fgets($this->file);
    }

    public function readFile()
    {
        while (!feof($this->file)) {
            yield $this->getRow();
        }
    }
}
