<?php

use LogFile\LogReader;
use PHPUnit\Framework\TestCase;

class LogReaderTest extends TestCase
{
    private $logreader;
    private $fileName;

    public function setUp() : void
    {
        $this->fileName = 'testFiles/log-processor_test.log';
        $this->logreader = new LogReader($this->fileName);
    }

    public function  testReadFile()
    {
        $file = $this->logreader->readFile();
        $this->assertTrue($file !== null);
    }
}
