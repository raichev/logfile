<?php

use LogFile\ErrorLogger;
use PHPUnit\Framework\TestCase;

class ErrorLoggerTest extends TestCase
{
    private $errorLogger;
    private $fileName;

    public function setUp() : void
    {
        $this->fileName = 'log-processor_test.log.err';
        $this->errorLogger = new ErrorLogger($this->fileName);
    }

    public function testWriteInFile()
    {
        $errorData = 'error1';
        $this->errorLogger->log($errorData);
        $this->assertFileExists($this->fileName);
        $this->assertFileIsReadable($this->fileName);
        $this->assertFileIsWritable($this->fileName);
    }

    public function tearDown(): void
    {
        if(file_exists($this->fileName)) {
            unlink($this->fileName);
        }
    }
}
