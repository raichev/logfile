<?php

use LogFile\OutputterJSON;
use PHPUnit\Framework\TestCase;

class OutputterJSONTest extends TestCase
{
    private $outputerFile;
    private $fileName;

    public function setUp() : void
    {
        $this->fileName = 'log-processor_test.json';
        $this->outputerFile = new OutputterJSON($this->fileName);
    }

    public function testGenerateFile()
    {
        $data = [1,2,3,4,5];
        $this->outputerFile->generate($data);
        $this->assertFileExists($this->fileName);
        $this->assertFileIsReadable($this->fileName);
        $this->assertFileIsWritable($this->fileName);
    }

    public function tearDown(): void
    {
        if(file_exists($this->fileName)) {
            unlink($this->fileName);
        }
    }
}
