<?php

use LogFile\LogProcessor;
use LogFile\OutputterJSON;
use LogFile\ErrorLogger;
use LogFile\LogReader;
use PHPUnit\Framework\TestCase;

class LogProcessorTest extends TestCase
{

    public function testProcess()
    {
        $logReader = $this->createMock(LogReader::class);

        $logReader->expects($this->any())
            ->method('readFile')
            ->willReturn([
                '2014-04-02 02:12:09 0 113.67.181.139 2863 68.232.40.109 80 TCP_HIT/200 3125 GET http://images.chem.co.uk/SMA-Wysoy-Soya-Infant-Formula-192594.jpg?o=gL1biz9dm5vMSjIVW9Npa@nLNCIj&V=iIxj&h=97&w=97&q=80 - 0 1214 "-" "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36" 26792 "http://results.chemistdirect.co.uk/search?p=Q&lbc=chemistdirect-uk&uid=18973090&ts=redesign&w=Baby&isort=score&method=and&view=grid&af=department:babychildcare&format=cat1"',
                '2014-04-03 02:25:09 0 113.67.181.139 2863 68.232.40.109 80 TCP_HIT/200 3125 GET http://images.chem.co.uk/SMA-Wysoy-Soya-Infant-Formula-192594.jpg?o=gL1biz9dm5vMSjIVW9Npa@nLNCIj&V=iIxj&h=97&w=97&q=80 - 0 1214 "-" "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36" 26792 "http://results.chemistdirect.co.uk/search?p=Q&lbc=chemistdirect-uk&uid=18973090&ts=redesign&w=Baby&isort=score&method=and&view=grid&af=department:babychildcare&format=cat1"',
                '2014-04-0 0 113.67.181.139 2863 68.232.40.109 80 TCP_HIT/200 3125 GET http://images.chem.co.uk/SMA-Wysoy-Soya-Infant-Formula-192594.jpg?o=gL1biz9dm5vMSjIVW9Npa@nLNCIj&V=iIxj&h=97&w=97&q=80 - 0 1214 "-" "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36" 26792 "http://results.chemistdirect.co.uk/search?p=Q&lbc=chemistdirect-uk&uid=18973090&ts=redesign&w=Baby&isort=score&method=and&view=grid&af=department:babychildcare&format=cat1"',

            ]);

        $errorLogger = $this->createMock(ErrorLogger::class);
        $errorLogger->expects($this->once())
            ->method('log');

        $outputter = $this->createMock(OutputterJSON::class);
        $outputter->expects($this->once())
            ->method('generate')
            ->willReturn($this->returnSelf());

        $logProcessor = new LogProcessor($logReader, $errorLogger, $outputter);
        $logProcessor->process();
    }
}